<?php

namespace App\Entity;

use App\Repository\ArtisteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\UniqueEntity;

#[UniqueEntity('nom')]
#[ORM\Entity(repositoryClass: ArtisteRepository::class)]
class Artiste
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\ManyToMany(targetEntity: Album::class, mappedBy: 'idArtiste')]
    private Collection $Album;

    #[ORM\ManyToMany(targetEntity: Musique::class, mappedBy: 'idArtiste')]
    private Collection $MusiquesList;

    public function __construct()
    {
        $this->Album = new ArrayCollection();
        $this->MusiquesList = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection<int, Album>
     */
    public function getAlbum(): Collection
    {
        return $this->Album;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->Album->contains($album)) {
            $this->Album->add($album);
            $album->addIdArtiste($this);
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        if ($this->Album->removeElement($album)) {
            $album->removeIdArtiste($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Musique>
     */
    public function getMusiquesList(): Collection
    {
        return $this->MusiquesList;
    }

    public function addMusiquesList(Musique $musiquesList): self
    {
        if (!$this->MusiquesList->contains($musiquesList)) {
            $this->MusiquesList->add($musiquesList);
            $musiquesList->addIdArtiste($this);
        }

        return $this;
    }

    public function removeMusiquesList(Musique $musiquesList): self
    {
        if ($this->MusiquesList->removeElement($musiquesList)) {
            $musiquesList->removeIdArtiste($this);
        }

        return $this;
    }

}
