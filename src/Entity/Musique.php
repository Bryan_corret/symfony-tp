<?php

namespace App\Entity;

use App\Repository\MusiqueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MusiqueRepository::class)]
class Musique
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $NomMusique = null;

    #[ORM\Column]
    private ?int $dateSortie = null;

    #[ORM\ManyToMany(targetEntity: Artiste::class, inversedBy: 'MusiquesList')]
    private Collection $idArtiste;

    #[ORM\OneToMany(mappedBy: 'MusiqueList', targetEntity: Album::class)]
    private Collection $IdAlbum;

    #[ORM\ManyToMany(targetEntity: Genres::class, inversedBy: 'musiques')]
    private Collection $genres;

    public function __construct()
    {
        $this->idArtiste = new ArrayCollection();
        $this->IdAlbum = new ArrayCollection();
        $this->genres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomMusique(): ?string
    {
        return $this->NomMusique;
    }

    public function setNomMusique(string $NomMusique): self
    {
        $this->NomMusique = $NomMusique;

        return $this;
    }

    public function getDateSortie(): ?int
    {
        return $this->dateSortie;
    }

    public function setDateSortie(int $dateSortie): self
    {
        $this->dateSortie = $dateSortie;

        return $this;
    }

    /**
     * @return Collection<int, Artiste>
     */
    public function getIdArtiste(): Collection
    {
        return $this->idArtiste;
    }

    public function addIdArtiste(Artiste $idArtiste): self
    {
        if (!$this->idArtiste->contains($idArtiste)) {
            $this->idArtiste->add($idArtiste);
        }

        return $this;
    }

    public function removeIdArtiste(Artiste $idArtiste): self
    {
        $this->idArtiste->removeElement($idArtiste);

        return $this;
    }

    /**
     * @return Collection<int, Album>
     */
    public function getIdAlbum(): Collection
    {
        return $this->IdAlbum;
    }

    public function addIdAlbum(Album $idAlbum): self
    {
        if (!$this->IdAlbum->contains($idAlbum)) {
            $this->IdAlbum->add($idAlbum);
            $idAlbum->setMusiqueList($this);
        }

        return $this;
    }

    public function removeIdAlbum(Album $idAlbum): self
    {
        if ($this->IdAlbum->removeElement($idAlbum)) {
            // set the owning side to null (unless already changed)
            if ($idAlbum->getMusiqueList() === $this) {
                $idAlbum->setMusiqueList(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Genres>
     */
    public function getGenres(): Collection
    {
        return $this->genres;
    }

    public function addGenre(Genres $genre): self
    {
        if (!$this->genres->contains($genre)) {
            $this->genres->add($genre);
        }

        return $this;
    }

    public function removeGenre(Genres $genre): self
    {
        $this->genres->removeElement($genre);

        return $this;
    }
}
