<?php

namespace App\Entity;

use App\Repository\AlbumRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AlbumRepository::class)]
class Album
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToMany(targetEntity: Artiste::class, inversedBy: 'Album')]
    private Collection $idArtiste;

    #[ORM\Column]
    private ?int $dateSortie = null;

    #[ORM\Column(length: 255)]
    private ?string $nomAlbum = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $img = null;

    #[ORM\ManyToOne(inversedBy: 'IdAlbum')]
    private ?Musique $MusiqueList = null;

    public function __construct()
    {
        $this->idArtiste = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Artiste>
     */
    public function getIdArtiste(): Collection
    {
        return $this->idArtiste;
    }

    public function addIdArtiste(Artiste $idArtiste): self
    {
        if (!$this->idArtiste->contains($idArtiste)) {
            $this->idArtiste->add($idArtiste);
        }

        return $this;
    }

    public function removeIdArtiste(Artiste $idArtiste): self
    {
        $this->idArtiste->removeElement($idArtiste);

        return $this;
    }

    public function getDateSortie(): ?int
    {
        return $this->dateSortie;
    }

    public function setDateSortie(int $dateSortie): self
    {
        $this->dateSortie = $dateSortie;

        return $this;
    }

    public function getNomAlbum(): ?string
    {
        return $this->nomAlbum;
    }

    public function setNomAlbum(string $nomAlbum): self
    {
        $this->nomAlbum = $nomAlbum;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(?string $img): self
    {
        $this->img = $img;

        return $this;
    }

    public function getMusiqueList(): ?Musique
    {
        return $this->MusiqueList;
    }

    public function setMusiqueList(?Musique $MusiqueList): self
    {
        $this->MusiqueList = $MusiqueList;

        return $this;
    }
}
