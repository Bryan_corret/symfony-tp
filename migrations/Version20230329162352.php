<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230329162352 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE album (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, musique_list_id INTEGER DEFAULT NULL, date_sortie INTEGER NOT NULL, nom_album VARCHAR(255) NOT NULL, img VARCHAR(255) DEFAULT NULL, CONSTRAINT FK_39986E43A0168068 FOREIGN KEY (musique_list_id) REFERENCES musique (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_39986E43A0168068 ON album (musique_list_id)');
        $this->addSql('CREATE TABLE album_artiste (album_id INTEGER NOT NULL, artiste_id INTEGER NOT NULL, PRIMARY KEY(album_id, artiste_id), CONSTRAINT FK_C9D0685D1137ABCF FOREIGN KEY (album_id) REFERENCES album (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_C9D0685D21D25844 FOREIGN KEY (artiste_id) REFERENCES artiste (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_C9D0685D1137ABCF ON album_artiste (album_id)');
        $this->addSql('CREATE INDEX IDX_C9D0685D21D25844 ON album_artiste (artiste_id)');
        $this->addSql('CREATE TABLE artiste (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE genres (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom_genre VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE musique (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nom_musique VARCHAR(255) NOT NULL, date_sortie INTEGER NOT NULL)');
        $this->addSql('CREATE TABLE musique_artiste (musique_id INTEGER NOT NULL, artiste_id INTEGER NOT NULL, PRIMARY KEY(musique_id, artiste_id), CONSTRAINT FK_1BFA3AC25E254A1 FOREIGN KEY (musique_id) REFERENCES musique (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_1BFA3AC21D25844 FOREIGN KEY (artiste_id) REFERENCES artiste (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_1BFA3AC25E254A1 ON musique_artiste (musique_id)');
        $this->addSql('CREATE INDEX IDX_1BFA3AC21D25844 ON musique_artiste (artiste_id)');
        $this->addSql('CREATE TABLE musique_genres (musique_id INTEGER NOT NULL, genres_id INTEGER NOT NULL, PRIMARY KEY(musique_id, genres_id), CONSTRAINT FK_C20AA99325E254A1 FOREIGN KEY (musique_id) REFERENCES musique (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_C20AA9936A3B2603 FOREIGN KEY (genres_id) REFERENCES genres (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_C20AA99325E254A1 ON musique_genres (musique_id)');
        $this->addSql('CREATE INDEX IDX_C20AA9936A3B2603 ON musique_genres (genres_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE album');
        $this->addSql('DROP TABLE album_artiste');
        $this->addSql('DROP TABLE artiste');
        $this->addSql('DROP TABLE genres');
        $this->addSql('DROP TABLE musique');
        $this->addSql('DROP TABLE musique_artiste');
        $this->addSql('DROP TABLE musique_genres');
    }
}
